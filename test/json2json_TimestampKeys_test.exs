defmodule Json2JsonTestTimestampKeys do
  use ExUnit.Case, async: true
  use Plug.Test
  doctest Json2Json

  @opts Json2Json.Endpoint.init([])

  test "POST TimestampKeys" do
    data = %{}
    rules = [%{"action" => "TimestampKeysPlugin"}]
    body = Jason.encode!(%{"data" => data, "rules" => rules})

    conn = conn(:post, "/json2json", body)
      |> put_req_header("content-type", "application/json")

    conn = Json2Json.Endpoint.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert Map.keys(Jason.decode!(conn.resp_body)) == ["_created", "_last_update"]
  end

  test "POST TimestampKeys with _created" do #,
    data = %{"_created" => 1533042873, "_last_update" => 1533042873}
    rules = [%{"action" => "TimestampKeysPlugin"}]
    body = Jason.encode!(%{"data" => data, "rules" => rules})

    conn = conn(:post, "/json2json", body)
      |> put_req_header("content-type", "application/json")

    conn = Json2Json.Endpoint.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert Jason.decode!(conn.resp_body)["_created"] == 1533042873
    assert Jason.decode!(conn.resp_body)["_last_update"] > 1533042873
  end
end
