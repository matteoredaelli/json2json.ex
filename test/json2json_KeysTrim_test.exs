defmodule Json2JsonTestKeysTrim do
  use ExUnit.Case, async: true
  use Plug.Test
  doctest Json2Json

  @opts Json2Json.Endpoint.init([])

  test "POST KeysTrim" do
    data = %{"k1 " => " v1", " k2" => "v2 "}
    rules = [%{"action" => "KeysTrimPlugin", "mode" => "no_overwrite"}]
    body = Jason.encode!(%{"data" => data, "rules" => rules})

    conn = conn(:post, "/json2json", body)
      |> put_req_header("content-type", "application/json")

    conn = Json2Json.Endpoint.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert Jason.decode!(conn.resp_body) == %{"k1" => " v1", "k2" => "v2 "}
  end

end
