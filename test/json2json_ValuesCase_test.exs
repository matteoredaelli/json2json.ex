defmodule Json2JsonTestValuesCase do
  use ExUnit.Case, async: true
  use Plug.Test
  doctest Json2Json

  @opts Json2Json.Endpoint.init([])

  test "POST ValuesCaseUpper" do
    data = %{"k1" => "vv1", "K2" => "vv2"}
    rules = [%{"action" => "ValuesCasePlugin", "case" => "upper", "keys" => ["k1"]}]
    body = Jason.encode!(%{"data" => data, "rules" => rules})

    conn = conn(:post, "/json2json", body)
      |> put_req_header("content-type", "application/json")

    conn = Json2Json.Endpoint.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert Jason.decode!(conn.resp_body) == %{"k1" => "VV1", "K2" => "vv2"}
  end

  test "POST ValuesCaseLower" do
    data = %{"K1" => "VV1", "k2" => "VV2"}
    rules = [%{"action" => "ValuesCasePlugin", "case" => "lower", "keys" => ["K1"]}]
    body = Jason.encode!(%{"data" => data, "rules" => rules})

    conn = conn(:post, "/json2json", body)
      |> put_req_header("content-type", "application/json")

    conn = Json2Json.Endpoint.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert Jason.decode!(conn.resp_body) == %{"K1" => "vv1", "k2" => "VV2"}
  end

end
