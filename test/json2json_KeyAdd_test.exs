defmodule Json2JsonTestKeyAdd do
  use ExUnit.Case, async: true
  use Plug.Test
  doctest Json2Json

  @opts Json2Json.Endpoint.init([])

  test "POST KeyAdd" do #1533042873,
    data = %{"k1" => "v1"}
    rules = [%{"action" => "KeyAddPlugin", "key" =>  "k2", "value" => "v2", "mode" => "no_overwrite"}]
    body = Jason.encode!(%{"data" => data, "rules" => rules})

    conn = conn(:post, "/json2json", body)
      |> put_req_header("content-type", "application/json")

    conn = Json2Json.Endpoint.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert Jason.decode!(conn.resp_body) == %{"k1" => "v1", "k2" => "v2"}
  end

  test "POST KeyAdd key_regex" do #1533042873,
    data = %{"k1" => "v1"}
    rules = [%{"action" => "KeyAddPlugin", "key" =>  "k2", "value" => "v2", "key_regex" => "^k.", "mode" => "no_overwrite"}]
    body = Jason.encode!(%{"data" => data, "rules" => rules})

    conn = conn(:post, "/json2json", body)
      |> put_req_header("content-type", "application/json")

    conn = Json2Json.Endpoint.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert Jason.decode!(conn.resp_body) == %{"k1" => "v1", "k2" => "v2"}
  end

  test "POST KeyAdd key_regex ko" do #1533042873,
    data = %{"k1" => "v1"}
    rules = [%{"action" => "KeyAddPlugin", "key" =>  "k2", "value" => "v2", "key_regex" => "2$", "mode" => "no_overwrite"}]
    body = Jason.encode!(%{"data" => data, "rules" => rules})

    conn = conn(:post, "/json2json", body)
      |> put_req_header("content-type", "application/json")

    conn = Json2Json.Endpoint.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert Jason.decode!(conn.resp_body) == %{"k1" => "v1"}
  end

  test "POST KeyAdd key_regex value_regex" do #1533042873,
    data = %{"k1" => "v1"}
    rules = [%{"action" => "KeyAddPlugin",
               "key" =>  "k2",
               "value" => "v2",
               "key_regex" => "^k.",
               "value_regex" => "^v.",
               "mode" => "no_overwrite"}]
    body = Jason.encode!(%{"data" => data, "rules" => rules})

    conn = conn(:post, "/json2json", body)
      |> put_req_header("content-type", "application/json")

    conn = Json2Json.Endpoint.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert Jason.decode!(conn.resp_body) == %{"k1" => "v1", "k2" => "v2"}
  end

  test "POST KeyAdd key_regex value_regex ko" do #1533042873,
    data = %{"k1" => "v1"}
    rules = [%{"action" => "KeyAddPlugin",
               "key" =>  "k2",
               "value" => "v2",
               "key_regex" => "^k.",
               "value_regex" => "2$",
               "mode" => "no_overwrite"}]
    body = Jason.encode!(%{"data" => data, "rules" => rules})

    conn = conn(:post, "/json2json", body)
      |> put_req_header("content-type", "application/json")

    conn = Json2Json.Endpoint.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert Jason.decode!(conn.resp_body) == %{"k1" => "v1"}
  end

end
