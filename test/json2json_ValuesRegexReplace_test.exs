defmodule Json2JsonTestValuesRegexReplace do
  use ExUnit.Case, async: true
  use Plug.Test
  doctest Json2Json

  @opts Json2Json.Endpoint.init([])

  test "POST ValuesRegexReplace" do
    data = %{"k1" => "v1", "k2" => "v12"}
    rules = [%{"action" => "ValuesRegexReplacePlugin",
               "replace_with" =>  "11",
               "value_regex" => "1$"}]
    body = Jason.encode!(%{"data" => data, "rules" => rules})

    conn = conn(:post, "/json2json", body)
      |> put_req_header("content-type", "application/json")

    conn = Json2Json.Endpoint.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert Jason.decode!(conn.resp_body) == %{"k1" => "v11", "k2" => "v12"}
  end

  test "POST ValuesRegexReplace special characters" do
    data = %{"k1" => "'v1", "k2" => "v1 2"}
    rules = [%{"action" => "ValuesRegexReplacePlugin",
               "replace_with" =>  "",
               "value_regex" => "[ \n'\"]+"}]
    body = Jason.encode!(%{"data" => data, "rules" => rules})

    conn = conn(:post, "/json2json", body)
      |> put_req_header("content-type", "application/json")

    conn = Json2Json.Endpoint.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert Jason.decode!(conn.resp_body) == %{"k1" => "v1", "k2" => "v12"}
  end

end
