defmodule Json2JsonTestKeysCase do
  use ExUnit.Case, async: true
  use Plug.Test
  doctest Json2Json

  @opts Json2Json.Endpoint.init([])

  test "POST KeysCaseUpper" do
    data = %{"k1" => "v1"}
    rules = [%{"action" => "KeysCasePlugin", "case" => "upper", "mode" => "no_overwrite"}]
    body = Jason.encode!(%{"data" => data, "rules" => rules})

    conn = conn(:post, "/json2json", body)
      |> put_req_header("content-type", "application/json")

    conn = Json2Json.Endpoint.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert Jason.decode!(conn.resp_body) == %{"K1" => "v1"}
  end

  test "POST KeysCaseLower" do
    data = %{"K1" => "V1"}
    rules = [%{"action" => "KeysCasePlugin", "case" => "lower", "mode" => "no_overwrite"}]
    body = Jason.encode!(%{"data" => data, "rules" => rules})

    conn = conn(:post, "/json2json", body)
      |> put_req_header("content-type", "application/json")

    conn = Json2Json.Endpoint.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert Jason.decode!(conn.resp_body) == %{"k1" => "V1"}
  end

end
