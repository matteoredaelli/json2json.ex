defmodule Json2JsonTestPing do
  use ExUnit.Case, async: true
  use Plug.Test
  doctest Json2Json

  @opts Json2Json.Endpoint.init([])

  test "GET /ping" do
    # Create a test connection
    conn = conn(:get, "/ping")

    # Invoke the plug
    conn = Json2Json.Endpoint.call(conn, @opts)

    # Assert the response and status
    assert conn.state == :sent
    assert conn.status == 200
    assert conn.resp_body == "pong"
  end

end
