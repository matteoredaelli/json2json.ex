defmodule Json2JsonTestKeysRegexReplace do
  use ExUnit.Case, async: true
  use Plug.Test
  doctest Json2Json

  @opts Json2Json.Endpoint.init([])

  test "POST KeyRegexReplace" do
    data = %{"k1" => "v1", "k1a" => "v2"}
    rules = [%{"action" => "KeysRegexReplacePlugin", "replace_with" =>  "11", "key_regex" => "1$", "mode" => "no_overwrite"}]
    body = Jason.encode!(%{"data" => data, "rules" => rules})

    conn = conn(:post, "/json2json", body)
      |> put_req_header("content-type", "application/json")

    conn = Json2Json.Endpoint.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert Jason.decode!(conn.resp_body) == %{"k11" => "v1", "k1a" => "v2"}
  end

  test "POST KeyRegexReplace spaces and other strange characters" do
    data = %{" 'k1'   " => "v1", ' "k 2"\n' => "v2"}
    rules = [%{"action" => "KeysRegexReplacePlugin",
               "replace_with" =>  "",
               "key_regex" => "[ '\"\n\t]+",
               "mode" => "no_overwrite"}]
    body = Jason.encode!(%{"data" => data, "rules" => rules})

    conn = conn(:post, "/json2json", body)
      |> put_req_header("content-type", "application/json")

    conn = Json2Json.Endpoint.call(conn, @opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert Jason.decode!(conn.resp_body) == %{"k1" => "v1", "k2" => "v2"}
  end

end
