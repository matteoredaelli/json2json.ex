defmodule Json2Json.Mixfile do
  use Mix.Project

  def project do
    [app: :json2json,
     version: "0.3.1",
     elixir: "~> 1.6", # yours may differ
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     description: description(),
     package: package(),
     deps: deps()]
  end

  def application do
    [extra_applications: [:logger],
      applications: [:cowboy, :plug, :maplog],
      mod: {Json2Json, []}] # This tells OTP which module contains our main application, and any arguments we want to pass to it
  end

  defp description do
    """
    Library for manipulating map objects, useful for mdm systems
    """
  end

  defp package do
    [
      files: ["lib", "mix.exs", "README*", "LICENSE*"],
      maintainers: ["Matteo Redaelli"],
      licenses: ["GPL V3+"],
      links: %{"GitLab" => "https://gitlab.com/matteo.redaelli/json2json.ex"}
    ]
end
  # The version numbers listed here are latest at the time of writing, you
  # should check each project and use the latest version in your code.
  defp deps do
    [
      {:cowboy, "~> 2.4"},
      {:dialyxir, "~> 1.0.0-rc.3", only: [:dev], runtime: false},
      {:ex_doc, ">= 0.0.0", only: :dev},
      {:jason, "~> 1.1"},
      {:plug, "~> 1.6"},
      {:tmap, "~> 18.0"},
      {:maplog, "~> 0.1"},
      {:yaml_elixir, "~> 2.1.0"},
    ]
  end
end
