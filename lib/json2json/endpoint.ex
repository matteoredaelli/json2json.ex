# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

defmodule Json2Json.Endpoint do
  require TMap
  require Maplog
  use Plug.Router
  require Logger

  plug Plug.Logger
  # NOTE: The line below is only necessary if you care about parsing JSON
  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Jason

  plug :match
  plug :dispatch

  def init(options) do
    options
  end

  def start_link do
    port = Application.fetch_env!(:json2json, :port)
    {:ok, _} = Plug.Adapters.Cowboy2.http(__MODULE__, [], port: port)
  end

  get "/" do
    response = """
  _                         ____      _
 (_)  ___    ___    _ __   |___ \\    (_)  ___    ___    _ __
 | | / __|  / _ \\  | '_ \\    __) |   | | / __|  / _ \\  | '_ \\
 | | \\__ \\ | (_) | | | | |  / __/    | | \\__ \\ | (_) | | | | |
_/ | |___/  \\___/  |_| |_| |_____|  _/ | |___/  \\___/  |_| |_|
|__/                                |__/

https://gitlab.com/matteo.redaelli/json2json.ex
"""
    send_resp(conn, 200, response)
  end

  get "/ping" do
    send_resp(conn, 200, "pong")
  end

  post "/json2json" do
    {status, body} = {200, parser_json2json(conn.body_params)}
    send_resp(conn, status, body)
  end

  post "/maplog" do
    {status, body} = {200, Maplog.parse_map(conn.body_params)}
    send_resp(conn, status, Jason.encode!(body))
  end

  get "/maplog_dump" do
    {status, body} = {200, Maplog.dump}
    send_resp(conn, status, Jason.encode!(body))
  end

  @spec parser_json2json(map) :: map
  defp parser_json2json(map) do
    out =
      case MapUtils.all_required_keys?(map, ["data", "rules"]) do
        true -> TMap.apply_rules(map["data"], map["rules"])
        false -> %{:error => "Missing required keys"}
      end
    Jason.encode!(out)
  end

end
