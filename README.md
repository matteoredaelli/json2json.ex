# Json2Json

To start the app:

  * Install dependencies with `mix deps.get`
  * Start web server with `iex -S mix`

Sanity check:

Visit [http://localhost:4000/ping](http://localhost:4000/ping) in your browser, you should see a "pong" message.

## Tests

Run `mix test`

## Sample urls
curl  -H "Content-Type: application/json"  -XPOST --data @test.json http://localhost:4000/json2json
